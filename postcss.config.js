const presetEnv = require('postcss-preset-env');

module.exports = {
	plugins: [
		presetEnv({
			/**
       * See features in https://preset-env.cssdb.org/features
       * WARNING! Features below stage 3 are way too fickle to
       * use in production! In this workshop, we rely on the
       * experimental nesting support (Stage 1) in order to
       * make it easier seeing the parallel between CSS and SCSS.
       */
			stage: 1
		})
	]
};
