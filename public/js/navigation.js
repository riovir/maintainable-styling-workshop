initNav({
	backSelector: 'a.js-back-link',
	nextSelector: 'a.js-next-link',
	slideHtmls: [
		'index.html',
		'goal.html',
		'non-goals.html',
		'tooling.html',
		'discover.html',
		'bem-names.html',
		'additional-rules.html',
		'activity-find.html',
		'activity-create.html',
		'activity-extract.html',
		'links.html',
	],
});

/**
 * Primitive navigation support.
 */
function initNav({ backSelector, nextSelector, slideHtmls, location = window.location }) {
	const currentHtml = htmlOf(location);
	const index = slideHtmls.indexOf(currentHtml);
	initAnchor({
		location,
		element: document.querySelector(backSelector),
		html: slideHtmls[index - 1]
	});
	initAnchor({
		location,
		element: document.querySelector(nextSelector),
		html: slideHtmls[index + 1]
	});
}

function initAnchor({ element, location: { pathname }, html }) {
	if (!element) { return; }
	if (!html) {
		element.setAttribute('style', 'opacity: 0;');
		element.setAttribute('disabled', '');
		element.setAttribute('aria-hidden', '');
		return;
	}
	const href = toHref({ location, html });
	element.setAttribute('href', href);
}

function toHref({ location: { pathname }, html }) {
	if (pathname.endsWith('/')) { return `${pathname}${html}`; }
	if (!pathname.endsWith('.html')) { return `${pathname}/${html}`; }
	return pathname.replace(/(.*)\/[^\.]+\.html$/, `$1/${html}`);
}

function htmlOf({ pathname = '/' }) {
	if (!pathname || pathname.endsWith('/')) { return 'index.html' }
	return pathname.replace(/.*\/([^\.]+\.html)$/, '$1');
}
