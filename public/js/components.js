/**
 * Unless you are specifically interested and familiar with Pika Web,
 * I suggest to ignore this file. It simply allows
 *
 * <demo-icon icon="something"></demo-icon>
 *
 * to be used in this workshop. Available icon names are here:
 * https://fontawesome.com/icons?d=gallery&s=solid&m=free
 */
import { addIcon } from 'https://cdn.pika.dev/@riovir/demo-components@^0.2.3';
import * as allIcons from 'https://cdn.pika.dev/@fortawesome/free-solid-svg-icons@^5.12.0';

addIcon(allIcons);
